import InvitationStyled from "./partial.styles";

const InvitationOne = () => {
  return (
    <InvitationStyled step="1">
      <p>
        “Dan di antara tanda-tanda kekuasaan-Nya ialah Dia menciptakan untukmu
        istri-istri dari jenismu sendiri, supaya kamu cenderung dan merasa
        tenteram kepadanya, dan dijadikan-Nya diantaramu rasa kasih dan sayang.“
      </p>
      <p>(Q.S Ar-Rum ayat 21)</p>
    </InvitationStyled>
  );
};

export default InvitationOne;
