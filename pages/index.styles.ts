import { mq } from "@styles/breakpoints";
import styled from "@emotion/styled";
import { keyframes } from "@emotion/core";
interface IProps {
  section?: string;
  isActive?: boolean;
  isTopHeader?: boolean;
  isOpenButton?: boolean;
  isSent?: boolean;
}

export const fadeIn = keyframes({
  "100%": {
    opacity: 1,
    filter: "blur(0)",
  },
});

export const WelcomeWrapper = styled.div(
  mq({
    backgroundImage: [
      "url(https://res.cloudinary.com/caturteguh/image/upload/v1673935176/farahwedding/bg_vsmlgi.jpg)",
      undefined,
      "url(https://res.cloudinary.com/caturteguh/image/upload/v1673953122/farahwedding/bg-mobile_vldage.jpg)",
    ],
    backgroundPosition: "center",
    backgroundSize: "cover",
    display: "flex",
    flexDirection: "column",
    gridTemplateColumns: "auto",
    alignItems: "center",
    justifyContent: "center",
    minHeight: "100vh",
    textAlign: "center",
    "&:before": {
      content: "''",
      width: "100vw",
      height: "100vh",
      backgroundColor: [undefined, "rgba(255,255,255,0.3)"],
      position: "absolute",
      top: 0,
      left: 0,
      zIndex: 1,
    },
  })
);

export const Welcome = styled.div(
  mq({
    position: "relative",
    zIndex: 2,
    ".title": {
      fontWeight: 400,
      fontSize: [24, undefined, 18],
      color: "#623634",
    },
    ".name": {
      padding: [24, undefined, "20px 0 48px 0"],
      display: "inline-flex",
      alignItems: "center",
      justifyContent: "center",
      gap: [40, undefined, 0],
      flexDirection: [undefined, undefined, "column"],
      h2: {
        fontWeight: 500,
        fontSize: [100, undefined, 60],
        color: "#653937",
        ".ampersand": {
          fontSize: [undefined, undefined, 60],
          color: "#eaba99",
        },
        span: {
          display: "inline-block",
          opacity: 0,
          filter: "blur(4px)",
          transition: "all 1s ease-in",
          userSelect: "none",

          "&:nth-of-type(1)": {
            animation: `${fadeIn} 0.8s 0.1s forwards cubic-bezier(0.11, 0, 0.5, 0)`,
          },
          "&:nth-of-type(2)": {
            animation: `${fadeIn} 0.8s 0.2s forwards cubic-bezier(0.11, 0, 0.5, 0)`,
          },
          "&:nth-of-type(3)": {
            animation: `${fadeIn} 0.8s 0.3s forwards cubic-bezier(0.11, 0, 0.5, 0)`,
          },
          "&:nth-of-type(4)": {
            animation: `${fadeIn} 0.8s 0.4s forwards cubic-bezier(0.11, 0, 0.5, 0)`,
          },
          "&:nth-of-type(5)": {
            animation: `${fadeIn} 0.8s 0.5s forwards cubic-bezier(0.11, 0, 0.5, 0)`,
          },
          "&:nth-of-type(6)": {
            animation: `${fadeIn} 0.8s 0.6s forwards cubic-bezier(0.11, 0, 0.5, 0)`,
          },
          "&:nth-of-type(7)": {
            animation: `${fadeIn} 0.8s 0.7s forwards cubic-bezier(0.11, 0, 0.5, 0)`,
          },
          "&:nth-of-type(8)": {
            animation: `${fadeIn} 0.8s 0.8s forwards cubic-bezier(0.11, 0, 0.5, 0)`,
          },
          "&:nth-of-type(9)": {
            animation: `${fadeIn} 0.8s 0.9s forwards cubic-bezier(0.11, 0, 0.5, 0)`,
          },
          "&:nth-of-type(10)": {
            animation: `${fadeIn} 0.8s 1s forwards cubic-bezier(0.11, 0, 0.5, 0)`,
          },
          "&:nth-of-type(11)": {
            animation: `${fadeIn} 0.8s 1.1s forwards cubic-bezier(0.11, 0, 0.5, 0)`,
          },
          "&:nth-of-type(12)": {
            animation: `${fadeIn} 0.8s 1.2s forwards cubic-bezier(0.11, 0, 0.5, 0)`,
          },
          "&:nth-of-type(13)": {
            animation: `${fadeIn} 0.8s 1.3s forwards cubic-bezier(0.11, 0, 0.5, 0)`,
          },
          "&:nth-of-type(14)": {
            animation: `${fadeIn} 0.8s 1.4s forwards cubic-bezier(0.11, 0, 0.5, 0)`,
          },
          "&:nth-of-type(15)": {
            animation: `${fadeIn} 0.8s 1.5s forwards cubic-bezier(0.11, 0, 0.5, 0)`,
          },
          "&:nth-of-type(16)": {
            animation: `${fadeIn} 0.8s 1.6s forwards cubic-bezier(0.11, 0, 0.5, 0)`,
          },
          "&:nth-of-type(17)": {
            animation: `${fadeIn} 0.8s 1.7s forwards cubic-bezier(0.11, 0, 0.5, 0)`,
          },
          "&:nth-of-type(18)": {
            animation: `${fadeIn} 0.8s 1.8s forwards cubic-bezier(0.11, 0, 0.5, 0)`,
          },
          "&:nth-of-type(19)": {
            animation: `${fadeIn} 0.8s 1.9s forwards cubic-bezier(0.11, 0, 0.5, 0)`,
          },
          "&:nth-of-type(20)": {
            animation: `${fadeIn} 0.8s 2s forwards cubic-bezier(0.11, 0, 0.5, 0)`,
          },
        },
        ".font-Abhaya": {
          margin: ["0 40px", undefined, "16px 0"],
          fontSize: [undefined, undefined, 40],
          color: "#eaba99",
          position: "relative",
          left: [20, undefined, 0],
          top: [undefined, undefined, 5],
          display: [undefined, undefined, "block"],
        },
      },
    },
  })
);

export const DateStyled = styled.div(
  mq({
    position: "relative",
    zIndex: 2,
    ".month": {
      fontWeight: 400,
      fontSize: [40, undefined, 30],
      color: "#623634",
    },
    ".day": {
      padding: "20px 0",
      fontSize: [24, undefined, 20],
      display: "grid",
      gridTemplateColumns: ["160px auto 160px", undefined, "100px auto 100px"],
      alignItems: "center",
      textTransform: "uppercase",
      color: "#653937",

      "& > div": {
        padding: "8px 0",
        borderTop: "1px solid #d2967b",
        borderBottom: "1px solid #d2967b",
      },

      h3: {
        padding: ["0 24px", undefined, "0 18px"],
        fontSize: [72, undefined, 60],
      },
    },

    ".year": {
      fontSize: [32, undefined, 20],
      color: "#653937",
    },
  })
);

export const ButtonStyled = styled.div<IProps>(({ isOpenButton, isSent }) =>
  mq({
    marginTop: isOpenButton ? [32, undefined, 24] : isSent ? 0 : 40,
    position: "relative",
    zIndex: 10,

    button: {
      fontSize: isSent ? 14 : [18, undefined, 14],
      fontWeight: 600,
      minWidth: isSent ? "100%" : 200,
      height: isOpenButton ? [50, undefined, 40] : isSent ? 30 : 50,
      // backgroundColor: "#d2967b",
      backgroundColor: "#eaba99",
      color: "#653937",
      border: 0,
      borderRadius: 50,
      transition: "all 500ms",
      textTransform: "uppercase",
      cursor: "pointer",
      letterSpacing: isOpenButton ? "1px" : "2px",

      ...(isOpenButton && {
        padding: "0 32px",
        fontSize: [18, undefined, 14],
      }),

      "&:hover": {
        backgroundColor: "#653937",
        color: "#eaba99",
      },
    },
  })
);

export default Welcome;
