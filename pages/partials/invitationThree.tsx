import InvitationStyled from "./partial.styles";
import Countdown from "react-countdown";

const InvitationThree = () => {
  const renderer = ({ days, hours, minutes, seconds, completed }) => {
    if (completed) {
      // Render a completed state
      return "Expired";
    } else {
      // Render a countdown
      return (
        <div>
          <div>
            {days} <span>hari</span> {hours} <span>jam</span> {minutes}{" "}
            <span>menit</span> {seconds} <span>detik</span>
          </div>
          <strong className="font-parisienne">menuju akad & walimah</strong>
        </div>
      );
    }
  };

  return (
    <InvitationStyled step="3">
      <h3 className="font-parisienne">Akad & Walimah</h3>
      <h2>Jum&rsquo;at, 29 September 2023</h2>
      <h2>09.00 - 11.30</h2>
      <p>
        <strong>Restoran Harmadha Joglo</strong>
        <br />
        <span>Jl. Diponegoro No.21, Selosari, Kab. Magetan</span>
      </p>
      <Countdown date={"2023-09-29T01:02:03"} renderer={renderer} />
    </InvitationStyled>
  );
};

export default InvitationThree;
