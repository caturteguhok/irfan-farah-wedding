// export const breakpoints = {
//     desktop: '@media (max-width: 1550px)',
//     xl: '@media (max-width: 1200px)',
//     lg: '@media (max-width: 1000px)',
//     md: '@media (max-width: 800px)',
//     sm: '@media (max-width: 600px)',
//   }

import facepaint from "facepaint";

export const Breakpoints = [991.98, 767.98, 576.98, 479.98];
export const mq = facepaint(
 Breakpoints.map((bp) => `@media (max-width: ${bp}px)`)
);

export default Breakpoints;
