import { mq } from "@styles/breakpoints";
import styled from "@emotion/styled";
import { keyframes } from "@emotion/core";
interface IProps {
  section?: string;
  isActive?: boolean;
  isTopHeader?: boolean;
  isNo?: boolean;
}

interface IProps {
  step?: string;
}

export const fadeIn = keyframes({
  "0%": {
    opacity: 0,
    filter: "blur(4px)",
  },
  "100%": {
    opacity: 1,
    filter: "blur(0)",
  },
});

export const NameStyled = styled.div(
  mq({
    textAlign: "center",
    color: "#653937",
    display: [undefined, undefined, "flex"],
    flexDirection: [undefined, undefined, "column"],
    justifyContent: [undefined, undefined, "space-between"],
    minHeight: [undefined, undefined, "60vh"],

    ".name-intro": {
      paddingLeft: 0,
      paddingRight: 0,

      h2: {
        margin: "24px 0",
        fontWeight: 500,
        fontSize: [50, undefined, 40],
        color: "#653937",

        ".ampersand": {
          margin: "0 10px",
          fontSize: [40, undefined, 32],
          color: "#eaba99",
          position: "relative",
          left: 1,
        },
      },
    },

    "& > span": {
      marginTop: 32,
      fontSize: [20, undefined, 16],
      display: "block",
    },
    h1: {
      margin: ["10px 0 20px 0", undefined, "8px 0 20px 0"],
      fontSize: [28, undefined, 24],
      textTransform: "capitalize",
      lineHeight: ["32px", undefined, "26px"],
    },
  })
);

export const slideTop = keyframes({
  "0%": {
    opacity: 0,
    filter: "blur(20px)",
    transform: "translateY(-20px)",
  },
  "100%": {
    opacity: 1,
    filter: "blur(0)",
    transform: "translateY(0)",
  },
});

export const InvitationStyled = styled.div<IProps>(({ step }) =>
  mq({
    textAlign: "center",
    transition: "all 500ms",
    animation: `${slideTop} 0.4s cubic-bezier(0.250, 0.460, 0.450, 0.940) both`,

    h2: {
      marginBottom: 20,
      fontSize: 18,
      color: "#653937",
    },
    p: {
      paddingTop: step === "1" ? [undefined, undefined, 16] : undefined,
      color: "#653937",
      fontSize: step === "1" ? [28, undefined, 20] : 20,
      lineHeight: step === "1" ? ["32px", undefined, "24px"] : "24px",
      ...(step === "1" && {
        "& + p": {
          marginTop: [16, undefined, 0],
          marginBottom: 24,
          fontSize: 18,
        },
      }),
    },

    ...(step === "2" && {
      "& > p": {
        fontSize: [undefined, undefined, 16],
        lineHeight: [undefined, undefined, "18px"],
      },
    }),

    ...(step === "3" && {
      h3: {
        marginBottom: 24,
        fontSize: [32, undefined, 24],
        fontWeight: 500,
        color: "#653937",
      },
      h2: {
        fontSize: [40, undefined, 20],
        color: "#653937",

        "& + h2": {
          marginTop: 8,
        },
      },
      p: {
        marginTop: [24, undefined, 16],
        marginBottom: 16,
        fontSize: [undefined, undefined, 18],
        color: "#653937",
        lineHeight: ["28px", undefined, "24px"],

        strong: {
          fontWeight: 700,
        },
      },
      "& > div": {
        marginTop: [24, undefined, 16],
        marginBottom: 24,
        fontSize: [32, undefined, 20],
        color: "#653937",

        "& > div": {
          fontWeight: 700,
          display: "flex",
          alignItems: "center",
          justifyContent: "center",

          span: {
            margin: "0 10px 0 4px",
            fontSize: [18, undefined, 14],
            fontWeight: 500,
          },
        },
        strong: {
          fontSize: [24, undefined, 20],
        },
      },
    }),

    ...(step === "4" && {
      h3: {
        marginBottom: 24,
        fontSize: [32, undefined, 24],
        fontWeight: 500,
        color: "#653937",
      },
      ".wrapper": {
        marginBottom: 24,
        textDecoration: "none",
        overflow: "hidden",
        maxWidth: "100%",
        width: "100%",
        height: "50vh",

        "#embed-map-canvas": {
          maxWidth: "100%",
          width: "100%",
          height: "100%",

          iframe: {
            width: "100%",
            height: "100%",
            border: 0,
          },
        },
      },
    }),

    ...(step === "5" && {
      h3: {
        marginBottom: [24, undefined, 16],
        fontSize: [32, undefined, 24],
        fontWeight: 500,
        color: "#653937",
      },
      h2: {
        margin: ["32px 0", undefined, "24px 0"],
        fontSize: [32, undefined, 24],
        fontWeight: 500,
        color: "#653937",
        lineHeight: "42px",
      },
      p: {
        marginTop: 24,
        fontSize: [undefined, undefined, 18],
        color: "#653937",
        lineHeight: ["28px", undefined, "24px"],

        "&.hadits": {
          fontSize: [18, undefined, 14],
        },

        "&.thanks": {
          marginTop: [24, undefined, 16],
        },

        "& + p": {
          marginTop: 8,
        },

        "&:last-of-type": {
          marginTop: [20, undefined, 16],
          marginBottom: 24,
          fontWeight: 700,
        },
      },
    }),

    ...(step === "6" && {
      "& > h3": {
        marginBottom: [24, undefined, 16],
        fontSize: [32, undefined, 24],
        fontWeight: 500,
        color: "#653937",
      },
      "& > div": {
        paddingBottom: 16,
        display: "grid",
        gridTemplateColumns: ["1fr 1fr", undefined, "1fr"],
        gap: [32, undefined, 20],
        gridAutoFlow: [undefined, undefined, "dense"],

        form: {
          display: "flex",
          flexDirection: "column",
          gap: 12,
          order: [undefined, undefined, 2],

          "input, textarea, select": {
            padding: "12px 12px",
            fontSize: [18, undefined, 14],
            color: "#653937",
            backgroundColor: "white",
            border: 0,
            borderRadius: 4,

            "&:focus-visible": {
              outline: "1px solid #eaba99",
            },
          },

          select: {
            backgroundImage:
              "linear-gradient(45deg, transparent 50%, gray 50%), linear-gradient(135deg, gray 50%, transparent 50%), linear-gradient(to right, #ccc, #ccc)",
            backgroundPosition:
              "calc(100% - 20px) calc(1em + 2px), calc(100% - 15px) calc(1em + 2px), calc(100% - 2.5em) 0.5em",
            backgroundSize: "5px 5px, 5px 5px, 1px 1.5em",
            backgroundRepeat: "no-repeat",
            // reset
            margin: 0,
            boxSizing: "border-box",
            WebkitAppearance: "none",

            "&:focus": {
              backgroundImage:
                "linear-gradient(45deg, green 50%, transparent 50%),linear-gradient(135deg, transparent 50%, green 50%),linear-gradient(to right, #ccc, #ccc)",
              backgroundPosition:
                "calc(100% - 15px) 1em, calc(100% - 20px) 1em, calc(100% - 2.5em) 0.5em",
              backgroundSize: "5px 5px, 5px 5px, 1px 1.5em",
              backgroundRepeat: "no-repeat",
              outline: 0,
            },

            "&:-moz-focusring": {
              color: "transparent",
              textShadow: "0 0 0 #000",
            },
          },
        },

        "& > div, ul": {
          minHeight: [324, undefined, 0],
          maxHeight: [324, undefined, "40vh"],
          overflow: "hidden auto",

          "&::-webkit-scrollbar": {
            width: 3,
          },

          /* Track */
          "&::-webkit-scrollbar-track": {
            background: "#f1f1f140",
          },

          /* Handle */
          "&::-webkit-scrollbar-thumb": {
            background: "#00000030",
            transition: "all 500ms",
          },

          /* Handle on hover */
          "&::-webkit-scrollbar-thumb:hover": {
            background: "#dedede",
          },
        },

        "& > div": {
          display: "flex",
          // alignItems: "center",
          alignItems: "flex-start",
          justifyContent: "center",
          order: [undefined, undefined, 1],

          "& > strong": {
            fontSize: 24,
            color: "#653937",
          },
        },

        ul: {
          width: "100%",
          height: [undefined, undefined, "25vh"],

          li: {
            padding: ["16px", undefined, "14px 10px"],
            fontSize: 14,
            backgroundColor: "#65393730",

            "& > div": {
              display: "flex",
              justifyContent: "space-between",

              "& > div": {
                display: "flex",
                flexDirection: "column",
                alignItems: "flex-start",

                strong: {
                  fontWeight: 700,
                  fontSize: 15,
                  color: "#653937",
                  textTransform: "capitalize",
                  textAlign: "left",
                },

                small: {
                  fontSize: 12,
                  fontWeight: 500,
                  color: "#65393790",
                  textTransform: "capitalize",
                  textAlign: "left",
                  display: "block",
                },
              },

              "& + div": {
                marginTop: [12, undefined, 8],
                fontSize: 14,
                textAlign: "left",
                lineHeight: "18px",
              },
            },

            "&:nth-child(odd)": {
              // marginTop: 16,
              // paddingTop: 16,
              // borderTop: "1px solid #dedede",

              backgroundColor: "#65393720",
            },
          },
        },

        ".empty": {
          padding: [undefined, undefined, 20],
          alignItems: "center",
          backgroundColor: "#65393730",

          strong: {
            fontSize: [undefined, undefined, 20],
            lineHeight: [undefined, undefined, "24px"],
          },
        },
      },
    }),

    ...(step === "7" && {
      padding: "20px 0 40px 0",
      color: "#653937",
      p: {
        marginBottom: 20,
        fontSize: 20,
      },
      h2: {
        marginBottom: 0,
        fontSize: [40, undefined, 24],
        fontWeight: 700,
        color: "#653937",
        lineHeight: [undefined, undefined, "30px"],
      },
      span: {
        margin: "20px 0",
        fontSize: [50, undefined, 40],
        color: "#eaba99",
        display: "block",
      },
    }),

    // Button Action
    "& + div": {
      "& > div": {
        marginTop: 32,
        display: "flex",
        alignItems: "center",
        justifyContent: "space-between",
        gap: 16,
        button: {
          padding: [undefined, undefined, "0 20px"],
          fontSize: [14, undefined, 16],
          fontWeight: 600,
          minWidth: [200, undefined, 0],
          height: 40,
          backgroundColor: "#eaba99",
          // color: ["#653937", undefined, "transparent"],
          color: "#653937",
          border: 0,
          borderRadius: 50,
          transition: "all 500ms",
          textTransform: "uppercase",
          cursor: "pointer",
          letterSpacing: "2px",

          "&:hover": {
            backgroundColor: "#653937",
            color: "#eaba99",
          },

          "&:disabled": {
            opacity: 0,
            cursor: "none",
            pointerEvents: "none",
          },

          // "&:before": {
          //   borderStyle: "solid",
          //   borderWidth: "0.25em 0.25em 0 0",
          //   content: "''",
          //   display: "inline-block",
          //   height: "0.45em",
          //   left: "0.15em",
          //   position: "relative",
          //   top: "0.15em",
          //   verticalAlign: "top",
          //   width: "0.45em",
          //   transform: "rotate(-135deg) translateX(-4px) translateY(2px)",
          // },

          // "& + button": {
          //   "&:before": {
          //     transform: "rotate(45deg) translateX(3px) translateY(-1px)",
          //   },
          // },
        },
      },
    },
  })
);

export const StepStyled = styled.div(
  mq({
    ol: {
      padding: 0,
      paddingBottom: 32,
      marginBottom: 32,
      transition: "all 500ms",
      position: "relative",
      display: [undefined, undefined, "none"],

      li: {
        padding: 0,
        minWidth: 0,
        width: "calc(100% / 7)",
        height: 3,
        borderBottom: 0,
        lineHeight: 1,

        "& > span": {
          display: "block",
          marginTop: 16,
          transition: "all 500ms",
        },

        "&:hover": {
          "& > span": {
            color: "#65393770",
          },
        },

        "&:before": {
          bottom: 0,
          content: "''",
          backgroundColor: "#65393750",
        },

        "&.go2150698616": {
          color: "#653937",
          transition: "all 500ms",
          "& > span": {
            display: [undefined, undefined, "none"],
          },

          "&:before": {
            left: 0,
            borderRadius: 0,
            height: 3,
            width: "100%",
            transition: "all 1000ms",
            position: [undefined, undefined, "absolute"],
          },
        },

        "&.go2150698616.go3842760039": {
          opacity: 1,

          "&:before": {
            backgroundColor: "#653937",
          },
          "& > span": {
            fontWeight: 700,
            color: "#653937",
            display: [undefined, undefined, "block"],
            position: [undefined, undefined, "absolute"],
            left: [undefined, undefined, "50%"],
            transform: [undefined, undefined, "translateX(-50%)"],
          },
        },
        "&.go2150698616.go433304200": {
          "&:before": {
            backgroundColor: "#653937",
          },
          "& > span": {
            fontWeight: 700,
            color: "#65393770",
          },
        },
      },
    },
  })
);

export const GridNameStyled = styled.div(
  mq({
    marginTop: [32, undefined, 24],
    marginBottom: [32, undefined, 24],
    // display: "inline-grid",
    // gridTemplateColumns: "1fr 40px 1fr",
    // textAlign: "center",
    "& > div": {
      // textAlign: "right",
      // maxWidth: 250,
      h1: {
        fontSize: [40, undefined, 28],
        fontWeight: 600,
        color: "#653937",
        letterSpacing: "1.4px",
        lineHeight: [undefined, undefined, "32px"],
      },

      p: {
        marginTop: 8,
        fontSize: [undefined, undefined, 18],

        strong: {
          fontWeight: 700,
        },
      },

      "& ~ div": {
        // textAlign: "left",
      },
    },

    ".ampersand": {
      margin: ["20px 0", undefined, "8px 0"],
      fontSize: [40, undefined, 32],
      color: "#eaba99",
    },
  })
);

export const AttendStyled = styled.span<IProps>(({ isNo }) =>
  mq({
    padding: "2px 8px 2px 6px",
    marginLeft: 10,
    fontSize: 12,
    fontWeight: 700,
    color: "white",
    backgroundColor: isNo ? "red" : "green",
    display: "inline-flex",
    alignItems: "center",
    gap: 4,
    borderRadius: 20,
    lineHeight: "16px",
    whiteSpace: "nowrap",

    svg: {
      width: 16,
      height: 16,
    },
  })
);

export default InvitationStyled;
