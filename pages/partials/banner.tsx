/* eslint-disable @next/next/no-img-element */
import { FunctionComponent, useState } from "react";
import {
  ButtonStyled,
  DateStyled,
  Welcome,
  WelcomeWrapper,
} from "../index.styles";
import { AnimatePresence } from "framer-motion";
import Modal from "@components/modal";
import InvitationOne from "./invitationOne";
import MultiStep from "react-multistep";
import { NameStyled, StepStyled } from "./partial.styles";
import InvitationTwo from "./invitationTwo";
import InvitationThree from "./invitationThree";
import InvitationFour from "./invitationFour";
import InvitationFive from "./invitationFive";
import { FlowerStyled } from "@components/modal/index.styles";
import { useWindowSize } from "utils/useWindowSize";
import { useRouter } from "next/router";
import InvitationSix from "./invitationSix";
import InvitationSeven from "./invitationSeven";
import AudioPlayer from "react-h5-audio-player";
import "react-h5-audio-player/lib/styles.css";

interface Props {
  handleOpenModal?: () => void;
}

const BannerPartial: FunctionComponent<Props> = ({ handleOpenModal }) => {
  const [modalOpen, setModalOpen] = useState(false);
  const [modalInvitation, setModalInvitation] = useState(true);
  const [startAudio, setStartAudio] = useState(false);
  const [src, setSrc] = useState("");

  const close = () => setModalOpen(false);
  const open = () => setModalOpen(true);
  const closeInvitation = () => {
    setModalInvitation(false);
    setSrc(
      "https://res.cloudinary.com/caturteguh/video/upload/v1693972175/farahwedding/audio_wedding_ilnw15.mp3"
    );
  };

  const playAudio = () => setStartAudio(!startAudio);

  const router = useRouter();
  const name = router.query?.name || "";

  // /?name=caturteguh

  const steps = [
    { title: "Pembukaan", component: <InvitationOne /> },
    { title: "Mempelai", component: <InvitationTwo /> },
    { title: "Akad", component: <InvitationThree /> },
    { title: "Peta", component: <InvitationFour /> },
    {
      title: "Doa",
      component: <InvitationFive />,
    },
    {
      title: "Ucapan",
      component: <InvitationSix />,
    },
    {
      title: "Berbahagia",
      component: <InvitationSeven />,
    },
  ];

  const size = useWindowSize();

  // const handleSetNewSrc = () => {
  //   setSrc(
  //     "https://res.cloudinary.com/caturteguh/video/upload/v1693972175/farahwedding/audio_wedding_ilnw15.mp3"
  //   );
  // };

  return (
    <>
      <WelcomeWrapper>
        <Welcome>
          <div className="title font-parisienne">Walimatul &rsquo;Urs</div>
          <div className="name">
            <h2 className="font-parisienne">
              <span>F</span>
              <span>a</span>
              <span>r</span>
              <span>a</span>
              <span>h</span>
              <span className="font-Abhaya">&</span>
              {/* </h2>
            <h2 className="ampersand">&</h2>
            <h2 className="font-parisienne"> */}{" "}
              <span>R</span>
              <span>i</span>
              <span>j</span>
              <span>a</span>
              <span>l</span>
            </h2>
          </div>
        </Welcome>
        <DateStyled>
          <div className="month font-parisienne">September</div>
          <div className="day">
            <div>Jum&rsquo;at</div>
            <h3>29</h3>
            <div>09.00 - 11.30</div>
          </div>
          <div className="year">2023</div>
        </DateStyled>
        <ButtonStyled>
          <button onClick={open}>
            <span className="font-Abhaya">Detail</span>
          </button>
        </ButtonStyled>
        <div style={{ position: "fixed", zIndex: 10, bottom: 40, right: 40 }}>
          <AudioPlayer
            autoPlay={true}
            onPlay={(e) => console.log("onPlay")}
            // src="https://res.cloudinary.com/caturteguh/video/upload/v1693972175/farahwedding/audio_wedding_ilnw15.mp3"
            showSkipControls={false}
            showJumpControls={false}
            showFilledProgress={false}
            showFilledVolume={false}
            showDownloadProgress={false}
            src={src}
          />
        </div>
      </WelcomeWrapper>
      <AnimatePresence initial={false} onExitComplete={() => null}>
        {modalInvitation && (
          <Modal isCardName>
            <NameStyled>
              <Welcome>
                <div className="title font-parisienne">
                  Walimatul &rsquo;Urs
                </div>
                <div className="name-intro">
                  <h2 className="font-parisienne">
                    <span>F</span>
                    <span>a</span>
                    <span>r</span>
                    <span>a</span>
                    <span>h</span>
                    <span className="ampersand font-Abhaya">&</span>
                    <span>R</span>
                    <span>i</span>
                    <span>j</span>
                    <span>a</span>
                    <span>l</span>
                  </h2>
                </div>
              </Welcome>
              <div>
                <span>Kepada Bapak/Ibu/Saudara/i</span>
                <h1>{name}</h1>
                {/* <h1>Catur Teguh Ok</h1> */}
              </div>
              <ButtonStyled isOpenButton>
                <button onClick={closeInvitation}>
                  <span className="font-Abhaya">Buka Undangan</span>
                </button>
              </ButtonStyled>
            </NameStyled>
          </Modal>
        )}
      </AnimatePresence>
      <AnimatePresence initial={false} onExitComplete={() => null}>
        {modalOpen && (
          <Modal handleClose={close}>
            <StepStyled>
              <MultiStep
                steps={steps}
                activeStep={0}
                prevButton={{
                  title: size.width < 479.98 ? "<" : "Kembali",
                }}
                nextButton={{
                  title: size.width < 479.98 ? ">" : "Lanjut",
                }}
              />
            </StepStyled>
            <FlowerStyled>
              <img
                src="https://res.cloudinary.com/caturteguh/image/upload/v1674360484/farahwedding/flower-top_aanxy4.png"
                alt="farah.wedding"
              />
            </FlowerStyled>
          </Modal>
        )}
      </AnimatePresence>
    </>
  );
};

export default BannerPartial;
