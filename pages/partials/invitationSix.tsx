import { ButtonStyled } from "pages/index.styles";
import InvitationStyled, { AttendStyled } from "./partial.styles";
import CheckIcon from "@components/icon/check";
import {
 collection,
 addDoc,
 onSnapshot,
 query,
 orderBy,
} from "firebase/firestore";
import { useEffect, useState } from "react";
import CrossIcon from "@components/icon/cross";
import { db } from "utils/firebase";
import { useWindowSize } from "utils/useWindowSize";
import dayjs from "dayjs";

import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

const getGuestbook = async () => {
 const res = await prisma.guestbook
  .findMany
  //   {
  //   select: {
  //    id: true,
  //    created_at: true,
  //    name: true,
  //    greetings: true,
  //    attendance: true,
  //   },
  //  }
  ();
 console.log(res, "res");
 return res;
};

const GuestBook = async () => {
 const guestbooks = await getGuestbook();

 return (
  <>
   <ul>
    {guestbooks.map((item, id) => {
     return (
      <li key={id}>
       <div>
        <div>
         <strong>{item.name}</strong>
         <small>
          {item.created_at
           ? dayjs(item.created_at).format("DD MMMM YYYY H:mm")
           : ""}
         </small>
        </div>
        <p>
         {item?.attendance === true ? (
          <AttendStyled>
           <CheckIcon />
           Hadir
          </AttendStyled>
         ) : (
          <AttendStyled isNo>
           <CrossIcon />
           Tidak Hadir
          </AttendStyled>
         )}
        </p>
       </div>
       <div dangerouslySetInnerHTML={{ __html: item.greetings }} />
      </li>
     );
    })}
   </ul>
   {/* <div className={guestbooks.length === 0 && "empty"}>
    {guestbooks.length === 0 ? (
     <strong>
      Silahkan isi formulir untuk mengirimkan ucapan & do&rsquo;a
     </strong>
    ) : (
     <ul>
      {guestbooks.map((item, id) => {
       return (
        <li key={id}>
         <div>
          <div>
           <strong>{item.name}</strong>
           <small>
            {item.created_at
             ? dayjs(item.created_at).format("DD MMMM YYYY H:mm")
             : ""}
           </small>
          </div>
          <p>
           {item?.attendance === true ? (
            <AttendStyled>
             <CheckIcon />
             Hadir
            </AttendStyled>
           ) : (
            <AttendStyled isNo>
             <CrossIcon />
             Tidak Hadir
            </AttendStyled>
           )}
          </p>
         </div>
         <div dangerouslySetInnerHTML={{ __html: item.greetings }} />
        </li>
       );
      })}
     </ul>
    )}
   </div> */}
  </>
 );
};

// const InvitationSix = () => {
//  const [newItem, setNewItem] = useState({
//   name: "",
//   note: "",
//   attend: "choose",
//   dateAdd: "",
//  });

//  const [items, setItems] = useState([]);

//  // Add item
//  const addItem = async (e) => {
//   e.preventDefault();

//   if (newItem.name !== "" && newItem.note !== "") {
//    await addDoc(collection(db, "items"), {
//     name: newItem.name.trim(),
//     note: newItem.note,
//     attend: newItem.attend,
//     dateAdd: dayjs().format(),
//    });

//    setNewItem({ name: "", note: "", attend: "", dateAdd: "" });
//   }
//  };

//  useEffect(() => {
//   const q = query(collection(db, "items"), orderBy("dateAdd", "desc"));

//   const unsubscribe = onSnapshot(q, (querySnapshot) => {
//    let itemsArr = [];
//    querySnapshot.forEach((doc) => {
//     itemsArr.push({ ...doc.data(), id: doc.id });
//    });
//    setItems(itemsArr);
//   });
//  }, []);

//  const size = useWindowSize();

//  return (
//   <InvitationStyled step="6">
//    <h3 className="font-parisienne">Ucapan & Doa</h3>
//    <div>
//     <form action="#">
//      <input
//       value={newItem.name}
//       onChange={(e) => setNewItem({ ...newItem, name: e.target.value })}
//       type="text"
//       placeholder="Nama"
//       className="font-Abhaya"
//       maxLength={50}
//      />
//      <textarea
//       value={newItem.note}
//       onChange={(e) => setNewItem({ ...newItem, note: e.target.value })}
//       rows={size.width < 767.98 ? 2 : 6}
//       placeholder="Ucapan untuk mempelai"
//       className="font-Abhaya"
//      />
//      <select
//       name="confirm"
//       id="confirm"
//       className="font-Abhaya"
//       defaultValue="choose"
//       onChange={(e) => setNewItem({ ...newItem, attend: e.target.value })}
//      >
//       <option value="choose" disabled defaultChecked defaultValue="">
//        Konfirmasi Kehadiran
//       </option>
//       <option value="hadir">Hadir</option>
//       <option value="tidak">Tidak Hadir</option>
//      </select>
//      <ButtonStyled isSent>
//       <button type="submit" onClick={addItem}>
//        <span className="font-Abhaya">Kirim</span>
//       </button>
//      </ButtonStyled>
//     </form>
//     <div className={items.length === 0 && "empty"}>
//      {items.length === 0 ? (
//       <strong>
//        Silahkan isi formulir untuk mengirimkan ucapan & do&rsquo;a
//       </strong>
//      ) : (
//       <ul>
//        {items.map((item, id) => {
//         console.log(item, "item");
//         return (
//          <li key={id}>
//           <div>
//            <div>
//             <strong>{item.name}</strong>
//             <small>
//              {item.dateAdd
//               ? dayjs(item.dateAdd).format("DD MMMM YYYY H:mm")
//               : ""}
//             </small>
//            </div>
//            <p>
//             {item?.attend === "hadir" ? (
//              <AttendStyled>
//               <CheckIcon />
//               Hadir
//              </AttendStyled>
//             ) : (
//              <AttendStyled isNo>
//               <CrossIcon />
//               Tidak Hadir
//              </AttendStyled>
//             )}
//            </p>
//           </div>
//           <div dangerouslySetInnerHTML={{ __html: item.note }} />
//          </li>
//         );
//        })}
//       </ul>
//      )}
//     </div>
//     {GuestBook}
//    </div>
//   </InvitationStyled>
//  );
// };

// export default InvitationSix;
export default GuestBook;
