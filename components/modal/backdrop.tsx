import { motion } from "framer-motion";

const Backdrop = ({ children, onClick }) => {
 return (
  <motion.div
   onClick={onClick}
   className="backdrop"
   initial={{ opacity: 0 }}
   animate={{ opacity: 1 }}
   exit={{ opacity: 0 }}
   style={{
    background: "rgba(0, 0, 0, 0.8)",
    width: "100%",
    height: "100%",
    position: "fixed",
    top: 0,
    left: 0,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    overflowY: "hidden",
    zIndex: 999999,
   }}
  >
   {children}
  </motion.div>
 );
};

export default Backdrop;
