import { useRouter } from "next/router";
import InvitationStyled, { GridNameStyled } from "./partial.styles";

const InvitationTwo = () => {
  const router = useRouter();
  const name = router.query?.name || "";

  return (
    <InvitationStyled step="2">
      <h2>بسم الله الرحمن الرحيم</h2>
      <p>
        Dengan memohon rahmat dan ridho Allah subhanahu wa ta&rsquo;ala kami
        bermaksud mengundang sekaligus mengharapkan do&rsquo;a dari
        Bapak/Ibu/Saudara/i pada pernikahan kami:
      </p>
      <GridNameStyled>
        <div>
          <h1 className="font-parisienne">Farah Avista Ali</h1>
          <p>
            Putri dari{" "}
            <strong>
              Bapak{" "}
              {name === "kerabat suwitorejo" ||
              name === "kerabat yusen" ||
              name === "kerabat nachi" ||
              name === "kerabat SMA 4"
                ? "Djoko Fitriyanto"
                : "Ali Taufik bin Ghodi"}
            </strong>{" "}
            dan <strong>Ibu Lia Amaliana</strong>
          </p>
        </div>
        <div className="ampersand">&</div>
        <div>
          <h1 className="font-parisienne">Muhammad Rijal Ramadhan</h1>
          <p>
            Putra dari <strong>Bapak La Ode Arsyad</strong> dan{" "}
            <strong>Ibu Hafshoh (rohimahallahu)</strong>
          </p>
        </div>
      </GridNameStyled>
    </InvitationStyled>
  );
};

export default InvitationTwo;
