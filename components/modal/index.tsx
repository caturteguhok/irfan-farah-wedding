import { motion } from "framer-motion";
import Backdrop from "./backdrop";
import ModalStyled from "./index.styles";
import { FunctionComponent } from "react";

interface IProps {
  handleClose?: () => void;
  children?: React.ReactNode;
  isCardName?: boolean;
}

const flip = {
  hidden: {
    transform: "scale(0) rotateX(-360deg)",
    opacity: 0,
    transition: {
      delay: 0.3,
    },
  },
  visible: {
    transform: " scale(1) rotateX(0deg)",
    opacity: 1,
    transition: {
      duration: 0.5,
    },
  },
  exit: {
    transform: "scale(0) rotateX(360deg)",
    opacity: 0,
    transition: {
      duration: 0.5,
    },
  },
};

const Modal: FunctionComponent<IProps> = ({
  handleClose,
  children,
  isCardName,
}) => {
  return (
    <Backdrop onClick={handleClose}>
      <ModalStyled isCardName={isCardName}>
        <motion.div
          onClick={(e) => e.stopPropagation()}
          variants={flip}
          initial="hidden"
          animate={"visible"}
          exit="exit"
        >
          {children}
        </motion.div>
      </ModalStyled>
    </Backdrop>
  );
};

export default Modal;
