import InvitationStyled from "./partial.styles";

const InvitationFour = () => {
  return (
    <InvitationStyled step="4">
      <h3 className="font-parisienne">Lokasi Restoran Harmadha Joglo</h3>
      <div className="wrapper">
        <div id="embed-map-canvas">
          <iframe src="https://www.google.com/maps/embed/v1/place?q=Restoran+Harmadha+Joglo,+Jalan+Diponegoro,+Nganten,+Selosari,+Magetan+Regency,+East+Java,+Indonesia&key=AIzaSyBFw0Qbyq9zTFTd-tUY6dZWTgaQzuU17R8"></iframe>
        </div>
      </div>
    </InvitationStyled>
  );
};

export default InvitationFour;
