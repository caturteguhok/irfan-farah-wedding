import { mq } from "@styles/breakpoints";
import styled from "@emotion/styled";
import { keyframes } from "@emotion/core";

interface IProps {
  section?: string;
  isActive?: boolean;
  isTopHeader?: boolean;
  isCardName?: boolean;
}

export const fadeIn = keyframes({
  "100%": {
    opacity: 1,
    filter: "blur(0)",
  },
});

export const ModalStyled = styled.div<IProps>(({ isCardName }) =>
  mq({
    margin: "0 auto",
    width: "90%",
    ...(isCardName && {
      minHeight: [undefined, undefined, "70vh"],
    }),

    "& > div": {
      margin: "0 auto",
      padding: ["30px 40px", undefined, "30px 20px 20px 20px"],
      ...(isCardName && { padding: "40px 20px 30px 20px" }),

      width: [
        isCardName ? 400 : 820,
        undefined,
        isCardName ? undefined : "90%",
        isCardName ? "90%" : undefined,
      ],
      backgroundColor: "white",
      // borderRadius: 16,
      position: "relative",
      overflow: "hidden",
      boxSizing: "border-box",

      "&:before": {
        content: "''",
        position: "absolute",
        width: "150%",
        height: "150%",
        top: "-25%",
        left: "-25%",
        zIndex: -1,
        background:
          "url(https://res.cloudinary.com/caturteguh/image/upload/v1674355703/farahwedding/bg-plain_koahgg.jpg) 0 0 repeat",
        transform: "rotate(0deg)",
        opacity: 0.8,
      },
    },
  })
);

export const FlowerStyled = styled.div(
  mq({
    position: "absolute",
    bottom: -30,
    left: "52%",
    transform: "translateX(-50%)",
    zIndex: -1,
    img: {
      width: 100,
      transform: "rotate(-30deg)",
    },
  })
);

export default ModalStyled;
