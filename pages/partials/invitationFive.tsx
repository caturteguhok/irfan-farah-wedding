import InvitationStyled from "./partial.styles";

const InvitationFive = () => {
  return (
    <InvitationStyled step="5">
      <h3 className="font-parisienne">Doa untuk Pengantin</h3>
      <h2>
        بَارَكَ اللهُ لَكَ وَبَارَكَ عَلَيْكَ وَجَمَعَ بَيْنَكُمَا فِي خَيْرٍ
      </h2>
      <p>
        “Semoga Allah memberkahimu di waktu bahagia dan memberkahimu di waktu
        susah, serta semoga Allah mempersatukan kalian berdua dalam kebaikan”.
      </p>
      <p className="hadits">(HR. Abu Dawud no. 2130)</p>
      <p className="thanks">
        Kehadiran dan doa Bapak/Ibu/Saudara/i atas pernikahan kami berdua
        merupakan suatu kehormatan dan kebahagiaan bagi kami.
      </p>
      <p>Jazakumullah Khairan Katsiran</p>
    </InvitationStyled>
  );
};

export default InvitationFive;
