import Seo from "@components/Seo";
import BannerPartial from "./partials/banner";
import { useEffect, useRef, useState } from "react";
import AudioPlayer from "react-h5-audio-player";
import "react-h5-audio-player/lib/styles.css";

const HomePage = () => {
  // if (typeof window !== "undefined") {
  //   window.addEventListener("DOMContentLoaded", () => {
  //     const audio = document.querySelector("audio");
  //     audio.volume = 0.3;
  //     audio.play();
  //   });
  // }

  const [audio, setAudio] = useState(() => {
    // Example audio samples from https://www.soundhelix.com/audio-examples
    let url =
      "https://res.cloudinary.com/caturteguh/video/upload/v1693972175/farahwedding/audio_wedding_ilnw15.mp3";
    return url;
  });

  const [startAudio, setStartAudio] = useState(false);
  const playAudio = () => setStartAudio(!startAudio);

  // const [src, setSrc] = useState("");
  // const handleSetNewSrc = () => {
  //   setSrc(
  //     "https://res.cloudinary.com/caturteguh/video/upload/v1693972175/farahwedding/audio_wedding_ilnw15.mp3"
  //   );
  // };

  return (
    <>
      <Seo title="Walimatul &rsquo;Urs Farah & Rijal" />
      {/* <audio
        src="https://res.cloudinary.com/caturteguh/video/upload/v1693972175/farahwedding/audio_wedding_ilnw15.mp3"
        controls={false}
        hidden
        autoPlay
        loop
      >
        <p>
          If you are reading this, it is because your browser does not support
          the audio element.
        </p>
      </audio> */}
      {/* <audio controls={false} hidden autoPlay loop>
        <source src={audio} type="audio/mpeg" />
        Your browser does not support the audio tag.
      </audio> */}

      {/* <div style={{ position: "fixed", zIndex: 10, bottom: 40, right: 40 }}>
        <AudioPlayer
          autoPlay={true}
          onPlay={(e) => console.log("onPlay")}
          // src="https://res.cloudinary.com/caturteguh/video/upload/v1693972175/farahwedding/audio_wedding_ilnw15.mp3"
          showSkipControls={false}
          showJumpControls={false}
          showFilledProgress={false}
          showFilledVolume={false}
          showDownloadProgress={false}
          src={src}
        />
        <button onClick={handleSetNewSrc}>Set new src</button>
      </div> */}
      <BannerPartial />
    </>
  );
};

export default HomePage;
