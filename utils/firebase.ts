// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
import { getFirestore } from "firebase/firestore";
// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyD3hYp19wa53on73tV1zY3kPpyLvI0Z1Cs",
  authDomain: "farahwedding-9a67d.firebaseapp.com",
  databaseURL: "https://farahwedding-9a67d-default-rtdb.firebaseio.com",
  projectId: "farahwedding-9a67d",
  storageBucket: "farahwedding-9a67d.appspot.com",
  messagingSenderId: "90309956847",
  appId: "1:90309956847:web:ba57dd5703a3e4204af71a",
};

// Initialize Firebase
const farahwedding = initializeApp(firebaseConfig);
export const db = getFirestore(farahwedding);
