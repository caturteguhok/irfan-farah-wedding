import { useRouter } from "next/router";
import InvitationStyled from "./partial.styles";

const InvitationSeven = () => {
  const router = useRouter();
  const name = router.query?.name || "";

  return (
    <InvitationStyled step="7">
      <p>Kami yang berbahagia:</p>
      <h2 className="font-parisienne">
        Keluarga Bapak{" "}
        {name === "kerabat suwitorejo" ||
        name === "kerabat yusen" ||
        name === "kerabat nachi" ||
        name === "kerabat SMA 4"
          ? "Djoko Fitriyanto"
          : "Ali Taufik bin Ghodi"}
      </h2>
      <span className="ampersand font-Abhaya">&</span>
      <h2 className="font-parisienne">Keluarga Bapak La Ode Arsyad</h2>
    </InvitationStyled>
  );
};

export default InvitationSeven;
